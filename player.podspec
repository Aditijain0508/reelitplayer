Pod::Spec.new do |s|
  s.name         = "Player"
  s.version      = "0.1.0"
  s.summary      = "Description of your project"
  s.description  = "Mandatorily longer description of your project"
  s.homepage     = "https://github.com/YourUserName/NameOfYourProject"
  
  s.license      = "Description of your licence, name or otherwise"
  s.author       = { "Your name occupation" => "your@email.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://github.com/YourUserName/YourProjectName.git", :tag => "#{s.version}" }
  s.source_files =  "Player/Classes/**/*.swift" # path to your classes. You can drag them into their own folder.
  s.dependency   'Core'
  s.requires_arc = true
  s.swift_version= '4.0'
  s.xcconfig     = { 'SWIFT_VERSION' => '4.0' }
  s.default_subspecs = 'Framework'
  s.subspec 'Framework' do |evernote|
    evernote.source_files = 'Player/Classes/**/*.{h,m,swift,storyboard}'
  end

end



