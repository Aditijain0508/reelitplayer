//
//  AppBaseRouter.swift
//  Core
//
//  Created by Aditi Jain 3 on 29/05/22.
//

import Foundation
import Core

public class PlayerRouter:NSObject{
    
    public static func routeTo(identifier:String)->UIViewController{
            let bundle = Bundle.init(identifier: "org.cocoapods.Player")
            let storyboard = UIStoryboard(name: "Player", bundle: bundle)
            let nxtVC = storyboard.instantiateViewController(withIdentifier: identifier)
            return nxtVC
    }
}
